import pytest
import json
from api.api import *

@pytest.fixture
def client():
    with app.test_client() as client:
        yield client

def test_get_users(client):
    response = client.get('/users')
    assert response.status_code == 200
    assert response.content_type == 'application/json'

def test_get_user_by_id(client):
    response = client.get('/user?id=1')
    assert response.status_code == 200
    assert response.content_type == 'application/json'

def test_get_user_by_id_in_path(client):
    response = client.get('/user/1')
    assert response.status_code == 200
    assert response.content_type == 'application/json'

def test_post_users(client):
    new_user = {'name': 'Test User', 'id': 100}
    response = client.post('/user', json=new_user)
    assert response.status_code == 200
    assert response.content_type == 'application/json'
    assert json.loads(response.data) == new_user
    assert len(users_dict) == 1

def test_put_users(client):
    updated_user = {'name': 'Updated User', 'id': 1}
    response = client.put('/user', json=updated_user)
    assert response.status_code == 200
    assert response.content_type == 'application/json'
    assert users_dict[0] == updated_user

def test_delete_users(client):
    response = client.delete('/user/1')
    assert response.status_code == 200
    assert response.content_type == 'application/json'
    assert len(users_dict) == 0