# 1. Base Image
FROM python:3.9-slim

# 2. Working Directory
WORKDIR /app

# 3. Copy Dependencies
COPY requirements.txt ./requirements.txt
RUN pip install -r requirements.txt

# 4. Copy Application Code
COPY . /app

# 5. Expose Port
EXPOSE 5000

# 6. Application Start Command
CMD [ "python", "app.py" ]
